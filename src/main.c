/**
 *
 * auditd protocol to HTTP/2 pipe.
 *
 * This process is run through auditspd, takes the auditd stream, translates it
 * to JSON, and sends it off to an HTTP/2 server specified by the configuration
 * file.
 */

#include <syslog.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <sys/mman.h>

#include "audit.h"

static const char *pgm = "auditp2"; /*! The program name for syslog. */

/**
 * The audit thread manager.
 */
static audit_t *audit_thr;

/**
 * If the frame_pool is initialized, shut it down.
 */
static void term_handler(int sig)
{
        if (audit_thr)
                frame_pool_shutdown(audit_thr->frame_pool);
}

/**
 * Set up the signal handler for termination.
 */
static inline void setup_signal() {
        struct sigaction sa;

        // register sighandlers
        sa.sa_flags = 0 ;
        sa.sa_handler = term_handler;
        sigemptyset( &sa.sa_mask ) ;
        sigaction( SIGTERM, &sa, NULL );
        sa.sa_handler = term_handler;
        sigemptyset( &sa.sa_mask ) ;
        sigaction( SIGCHLD, &sa, NULL );
        sa.sa_handler = SIG_IGN;
        sigaction( SIGHUP, &sa, NULL );
}

/*
 * main is started by auditd. See dispatcher in auditd.conf
 */
int main(int argc, char *argv[])
{
        /* Avoid holding resources and filesystems we're not using. */
        (void)chdir("/");
        close(0);
        close(1);
        close(2);

        /* Ensure we have valid stdio handles. */
        open("/dev/null", O_RDONLY);
        open("/dev/null", O_WRONLY);
        open("/dev/null", O_WRONLY);

        /* Lock our pages in memory so we stay resident. */
        mlockall(MCL_FUTURE);

        /* Open the syslog. */
        openlog(pgm, LOG_PID, LOG_DAEMON);
        syslog(LOG_INFO, "Initializing");

        // Start the main thread.
        audit_thr = audit_dispatch();

        if (audit_thr == NULL) {
                syslog(LOG_ERR, "Could not initialize audit receiver.");
                exit(1);
        }

        while (audit_wait(audit_thr) == 2)

        if (frame_pool_wait(frame_pool)) {
                syslog(LOG_INFO, "Shut down.");
                return 0;
        } else {
                syslog(LOG_ERR, "Shut down failed.");
                return 1;
        }
}
