/**
 * audit.c
 *
 * This reads the event stream from auditd, and queues them up for the HTTP/2
 * threads.
 *
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <locale.h>
#include <libaudit.h>
#include "audit.h"
#include "worker.h"
#include "frame_pool.h"

void audit_dispatch();
int recv_audit(int fd, frame_t *f);
static inline int audit_fd_open();
static inline int audit_ep_init(int fd);

static audit_t audit_thr;

/**
 * Initialize the frame_pool, the worker threads, and then run the dispatcher.
 *
 * Returns a pointer to audit_thr so the caller can wait and decide if
 * it should restart or terminate.
 */
audit_t *audit_start()
{
        audit_thr->frame_pool = frame_pool_init(131072);
        if (!audit_thr->frame_pool) {
                syslog(LOG_ERR, "audit_dispatch: could not initialize frame pool.");
                return NULL;
        }

        if (thrd_create(&audit_thr->audit_thr, audit_run, pool) != thrd_success) {
                log_error(__func__, "thrd_create: audit_run", errno);
                return NULL;
        }

        return &audit_thr;
}

/**
 * The audit thread itself.
 *
 * This initializes the audit session, then dispatches messages to the
 * workers.
 */
static inline int audit_run(frame_pool_t *pool, int ep, int fd)
{
        int fd = audit_fd(fd);

        if (fd < 0)
                return fd;

        int ep = audit_ep(fd);
        if (ep < 0) {
                close(fd);
                return ep;
        }

        int ret = AUDIT_THR_RETRY;
        struct epoll_event *events = TMALLOC(MAXEVENTS, struct epoll_event);
        while (frame_pool_running(pool)) {
                int nv = epoll_wait(epoll_fd, events, MAXEVENTS, -1);
                if (nv < 0) {
                        log_error(__func__, "epoll_wait failed. Requesting reconnection.", errno);
                        goto exit;
                }

                for (e = 0; e < nv; e++) {
                        if ((events[i].events & EPOLLERR) ||
                            (events[i].events & EPOLLHUP) ||
                            (!(events[i].events & EPOLLIN))) {
                                syslog(LOG_ERR, "Audit session disconnected. Requesting reconnection.");
                                goto exit;
                        } else if (events[i].data.fd == fd) {
                                int fs = recv_audit_all(fd, pool);

                                if (fs < 0) {
                                        syslog(LOG_ERR, "Failed to receive frames. Requesting reconnection.");
                                        goto exit;
                                } else if (fs >= 0) {
                                        char msg[1024];
                                        snprintf(msg, sizeof(msg), "%s: Received %d audit frames.", __func__, fs);
                                        syslog(LOG_INFO, msg);
                                }
                        }
                }
        }

exit:
        frame_pool_shutdown(pool);
        close(ev);
        close(fd);

        return ret;
}

/**
 * Read all available netlink frames and dispatch them.
 *
 * Returns the number of frames pulled when done, or <0 on error.
 */
int recv_audit_all(int fd, frame_pool *p)
{
        for(int n=0; ; n++) {
                frame_t *f = frame_pool_pop(pool);
                if (!f) {
                        syslog(LOG_ERR, __func__ ": Could not pop frame.");
                        return -1;
                }

                int s = recv_audit(fd, f);
                if (s == 1) {
                        return n;
                } else if (s < 0) {
                        log_error(__func__, "recv_audit", errno);
                        return s;
                } else {
                        syslog(LOG_NOTICE,"type=%d, payload size=%d",
                               hdr.type, hdr.size);
                        syslog(LOG_NOTICE,"data=\"%.*s\"", hdr.size,
                               (char *)data);
                }
        }
}

/**
 * Read a netlink frame.
 *
 * If a frame is returned, returns 0.
 * If there are no frames remaining, returns 1.
 * Otherwise returns -errno.
 */
int recv_audit(int fd, frame_t *f) {
        struct iovec vec[2];

        /* Get header first. it is fixed size */
        vec[0].iov_base = (void*)&hdr;
        vec[0].iov_len = sizeof(hdr);

        // Next payload
        vec[1].iov_base = data;
        vec[1].iov_len = MAX_AUDIT_MESSAGE_LENGTH;

        rc = readv(acb->pipe_fd, vec, 2);
        if (rc == E_WOULDBLOCK || rc == E_AGAIN) {
                return 1;
        } else if(rc <= 0) {
                return -e;
        } else {
                return 0;
        }
}

/**
 * Open a nonblocking session with the audit netlink.
 */
static inline int audit_fd_open() {
        int fd = audit_open();

        if (fd < 0) {
                log_errno(__func__, "audit_open", errno);
                return -1;
        }

        int flags = fcntl(fd, F_GETFL, 0);
        if (flags < 0)
                flags = 0;

        flags |= O_NONBLOCK;
        if (fcntl(fd, F_SETFL, flags) < 0) {
                log_errno(__func__, "Set O_NONBLOCK on audit fd", errno);
                close(fd);
                return -1;
        }

        return fd;
}

/**
 * Set up epoll for the audit_fd.
 *
 * Configures epoll to notify us of edge-triggered incoming events.
 */
static inline int audit_ep_init(int fd) {
        int ep = epoll_create(1);

        if (ep < 0) {
                log_errno(__func__, "epoll_create", errno);
                return ep;
        }

        struct epoll_event ev;
        ZERO_MEMD(ev, struct epoll_event);
        ev.data.fd = fd;
        ev.events = EPOLLIN | EPOLLET;
        if (epoll_ctl(ep, EPOLL_CTL_ADD, fd, &ev) < 0) {
                log_errno(__func__, "epoll_ctl", errno);
                close(ep);
                return -1;
        }

        return ep;
}
