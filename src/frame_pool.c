/**
 * frame_pool.c
 *
 * Frame pool allocation and handling. This is here so worker threads can
 * allocate their own frame pools when they start. This way they're in that
 * thread's storage, instead of in the main thread's.
 */

static frame_pool_t frame_pool;

inline static bool running_init(running_t *running);
inline static void running_del(running_t *running);

inline static bool frames_init(frames_t *frames);
inline static void frames_del(frames_t *frames);

inline static uint64_t gen_seq_nbr();

typedef union seq_init_t;

/**
 * Initialize the global frame pool if it isn't already. Returns its pointer on
 * success, and NULL on failure.
 */
frame_pool_t *frame_pool_init(uint32_t n_frames) {
        if(!frames_init(&(frame_pool.frames))) {
                return NULL;
        }

        if (!running_init(&(frame_pool.running))) {
                frames_del(&frame_pool.frames);
                return NULL;
        }

        return &frame_pool_t;
}

bool frame_pool_shutdown() {
        atomic_store(&(frame_pool.running.running), false);
        shutdown_wait(p);
}

/**
 * Wait for the pool to have fully shut down, then free it.
 *
 * This creates its own mtx_t so that frame_pool_shutdown can be
 * called from multiple threads, and the frame_pool_push function will
 * signal all waiters when finished.
 */
static bool frame_pool_shutdown_wait(); {
        mtx_t finished_m;

        if (mtx_init(&finished_m) != thrd_success)
                return false;

        mtx_lock(&finished_m);
        while (cnd_wait(&(frame_pool.running.shutdown_cnd), &finished_m))
                if (atomic_fetch(&frame_pool.running.completed))
                        break;

        ck_spinlock_lock(&(frame_pool.running.release_lock));
        if (!frame_pool.running.completed) {
                frames_del(frame_pool.frames);
                running_del(frame_pool.running);
        }
        ck_spinlock_unlock(&(frame_pool.running.release_lock));
}

int frame_pool_wait(const frame_pool_t *p)
{

}
/**
 * Initialize the running_t of the frame_pool.
 */
inline static bool running_init() {
        running_t *r = &(frame_pool.running);

        if (cnd_init(&r.shutdown_wait) != thrd_success) {
                return false;
        }

        atomic_store(r->running, true);
        atomic_store(r->completed, false);

        return true;
}

/**
 * Tear down a running_t.
 */
inline static void running_del() {
        running_t *r = &(frame_pool.running);

        mtx_destroy(&(r.stopped_m));
        cnd_destroy(&(r.stopped));
        r.completed = true;
}

/**
 * Allocate the frame descriptors and add them to the pool.
 */
inline static bool frames_init(frames_t *frames, uint64_t n) {
        /* Allocate the frame descriptors. */
        if ((frames->frames=GMALLOC(n, frame_t)) == NULL)
                return false;

        /** Add all the frames to the queue. */
        for(frame_t *f = p -> frames; (p->frames) - f < r; f++)
                ck_stack_push(&(p->stack), (p->frames), f);

        atomic_store(&(frames->n_frames), n);
        atomic_store(&(frames->n_frames_avail), n);
        atomic_store(&(frames->seq_nbr), gen_seq_nbr());

        return true;
}

/**
 * A contiguous block for reading in an initial random state.
 */
typedef union {
        char buf[4];
        uint32_t seq_init;
} seq_init_t;

/**
 * Initialize the sequence number from a cryptographically strong random value.
 *
 * 1. First we try gnutls to get a random seed.
 * 2. If that fails, we read 4 bytes from /dev/urandom into the middle of the
 * seed. This gives us a random seed, but a lower probability of
 * overflow. This will return 0 on error.
 */
inline static uint64_t gen_seq_nbr() {
        uint64_t seq_nbr = 0;

        int rnd = open("/dev/urandom", O_RDONLY);
        if (rnd < 0) {
                syslog(LOG_ERR, "Unable to open /dev/urandom.");
                return 0;
        }

        seq_init_t seq_init;
        rnd_seq.seq_init = 0;
        while (rnd_seq.seq_init == 0) {
                if (read(rnd, &(rnd_seq.buf), sizeof(rnd_seq.buf))) {
                        syslog(LOG_ERR, "Unable to read initial seed state.");
                        goto exit;
                }
        }

        seq_nbr = ((uint_64_t)(rnd_seq.seq_init & 0xFFFF0000)) << 48;
exit:
        close(rnd);
        return seq_nbr;
}
