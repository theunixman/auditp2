/** \file worker.c
 *
 * \brief Implementation of the worker threads.
 */

#include <stdint.h>
#include "fmalloc.h"
#include "worker.h"

/**
 * The global worker_t pool.
 */
static workers_t workers;

static int worker(worker_t *w);

/**
 * Initialize a worker and wait for it to start.
 *
 * Once it's started, it'll return the worker, or if it couldn't
 * initialize, it'll return NULL.
 */
static inline worker_t *worker_init(worker_t *w, const frame_pool_t *p)
{
        cnd_init(&w->w_started);
        mtx_init(&m);
        w->w_pool = p;
        w->w_thrd = thrd_create(worker, w);
        cnd_wait(&w->w_started, &m);
        if (!w->w_stack)
                return NULL;
        return w;
        }

}

bool start_workers(const frame_pool_t *p, uint32_t n);
{
        worker_t *ws = GMALLOC(n, worker_t);

        for(uint32_t i = 0; i < n; i ++) {
                if (worker_init(ws + i) == NULL) {
                        syslog(LOG_ERR, "Could not start worker thread.");
                        stop_workers(ws, n);
                        return NULL;
                }


        }

        return ws;
}

static void stop_workers(worker_t *ws, uint32_t n)
{
        for(uint32_t i = 0; i < n; i++) {
                worker_t w = ws[i];
                if (!w)
                        return;

        }
}

/**
 * A worker that dispatches frames as needed.
 */
static int worker(worker_t *w) {
        w->w_stack = frame_stack_init(w->w_pool);
        if (!w->w_stack) {
                syslog(LOG_ERR, "worker: could not init frame_stack_t");
                return -1;
        } else {
                syslog(LOG_INFO, "worker: starting");
        }

        cnd_signal(&w->w_started);

        char log_msg[1024];
        while(frame_t *f = frame_stack_pop(w->stack)) {
                snprintf(log_msg, sizeof(log_msg), "worker: received frame type %d", f->f_type);
                syslog(LOG_INFO, log_msg);
                frame_pool_push(f);
        }

        return 0;
}
