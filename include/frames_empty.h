/**
 * frames_empty.h - A frame_stack_t for the empty frames.
 */

/**
 * The global empty frames pool.
 *
 * This is where all the empty frames are kept. It also holds the global frame
 * sequence number, and the "running" flag.
 *
 * When the system is running, "running" is true, and pushes and pops are
 * allowed. When "running" is false, the system is shutting down, and only
 * pushes are allowed.
 */
typedef struct {
        volatile atomic_uint_fast64_t seq_nbr; /* The global frame sequence number. */
        const uint64_t n_frames; /* The total number of frames allocated. */
        const frame_t *frames; /* The actual frame data. */
} empty_frames_t;
