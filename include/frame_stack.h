/**
 * frame_stack.h
 *
 * Manage the per-thread work stacks.
 *
 */

#ifndef _frame_stack_h_
#define _frame_stack_h_

#include <libaudit.h>
#include <stdatomic.h>
#include <ck_stack.h>

#include "fmalloc.h"
#include "frame.h"
#include "frame_pool.h"

/**
 * A stack of frames. The next frame used will be the frame most likely in
 * cache.
 *
 * Each worker thread has its own stack, and will always be the only
 * consumer of frames, but any other thread can push frames onto any
 * frame stack, so we use the mpnc functions.
 */
typedef struct {
        volatile const frame_pool_t *pool; /*! The global frame pool. */

        volatile atomic_uint_fast64_t stack_len; /*! The length of the frames fifo. */
        ck_stack_t stack; /* The stack management structure. */
} frame_stack_t;

/**
 * Initializer for a frame_stack_t.
 */
#define FRAME_STACK_INITIALIZER {NULL, 0, CK_STACK_INITIALIZER}

/**
 * Initialize a frame_stack_t from the main frame pool.
 */
inline frame_stack_t *frame_stack_init(volatile const frame_pool_t *p) {
        frame_stack_t *s = TMALLOC(1, frame_stack_t, 0);

        if (s == NULL)
                return NULL;

        *s = FRAME_STACK_INITIALIZER;
        s->pool = p;
}

/**
 * Push all the frames onto the main pool, free resources, and exit.
 */
inline void frame_stack_del(const frame_stack_t *s) {
        atomic_store(&(s->stack_len), 0);

        frame_t *f = ck_stack_batch_pop_mpmc(s->stack);
        while(f) {
                frame_pool_push(s->pool, f);
                f = (frame_t *)f->f_entry.next;
        }
};

/**
 * Check if we're running, and dispose if we aren't.
 */
inline bool frame_stack_running(frame_stack_t *s, frame_t *f) {
        if (!frame_pool_running(s->pool)) {
                if (f)
                        frame_pool_push(s->pool, f);

                frame_stack_del(s);
                return false;
        }

        return true;
};

/**
 * Push a frame onto a worker queue if we're running.
 *
 * If not, stop, and return false.
 */
inline bool frame_stack_push(const frame_stack_t *s, frame_t *f) {
        if (!frame_stack_running(s, f))
                return false;

        ck_stack_push_mpnc(&s->stack, &f->f_entry);
        atomic_fetch_add(&s->stack_len, 1);
        return true;
};

/**
 * Pull the next available frame from the stack.
 *
 * If we can't pull a frame, we don't return a frame.
 */
inline frame_t *frame_stack_pop(const frame_stack_t *s) {
        if (!frame_stack_running(s, NULL))
                return NULL;

        frame_t *f = ck_stack_pop_mpmc(&(f->stack));

        atomic_fetch_add(&s->stack_len, -1);
        return f;
};

#endif
