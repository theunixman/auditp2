#ifndef _fmalloc_h
#define _fmalloc_h

#include <syslog.h>
#include <jemalloc/jemalloc.h>

/**
 * malloc a memory region with the extra mallocx flags, aligned to a
 * 4k boundary.
 *
 * By default, this will use a thread-managed arena and not fill the
 * memory.
 */
inline void *amalloc(size_t b, int o) {
        mallocx(b, MALLOCX_ALIGN(4096) | o);
};

/**
 * malloc space for n items of type t with tho additional options o.
 *
 * By default, this will use a thread-managed arena and not initialize
 * the memory.
 */
inline void *tmalloc(size_t n, size_t s, int o) {
        return amalloc(n * s, o);
};

/**
 * malloc a zero-filled memory region with the extra mallocx flags, aligned to
 * a 4k boundary.
 */
inline void *zmalloc(size_t n, size_t s, int o) {
        return tmalloc(n, s, MALLOCX_ZERO | o);
};

/**
 * malloc a region in the non-thread-managed region.
 */
inline void *gmalloc(size_t n, size_t s, int o) {
        return zmalloc(n, s, MALLOCX_TCACHE_NONE | o);
};

/**
 * Log errno from the specified function for the operation op.
 */

inline void log_errno(const char *fn, const char *op, int e) {
        syslog(LOG_ERR, "%s: Operation %s: %d: %s", fn, op, e, strerror(e));
};

#endif
