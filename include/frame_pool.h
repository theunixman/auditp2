/**
 * frames.h
 *
 * This is the main frame pool. All frames at some point are popped or pushed
 * from here by the various worker tasks. When a shutdown is requested, this
 * waits for all frames to be returned, then exits.
 */

#ifndef _frames_h_
#define _frames_h_

#include <libaudit.h>
#include <stdatomic.h>
#include <ck_spinlock.h>
#include <ck_stack.h>

#include "fmalloc.h"
#include "frame.h"

/**
 * Manages whether we're running or waiting for frames to return.
 *
 * While running is true, we accept pops and pushes. When it's set to
 * false, we don't accept pops, only pushes, and will exit the
 * listening thread when all frames are returned.
 */
typedef struct {
        ck_spinlock_t release_lock; /*! The spinlock guarding releasing resources. */
        cnd_t shutdown_cnd; /*! The condition variable all waiters for shutdown use. */
        atomic_bool running; /*! Controls the running state. */
        bool completed; /*! Lets the main process know we're
                         *  done. Guarded by release_lock. */
} running_t;

/**
 * Management for the actual frames.
 */
typedef struct {
        atomic_uint_fast64_t n_frames_avail; /*! The number of frames available. */
        uint64_t n_frames; /*! The number of frame descriptors in the pool. */
        atomic_uint_fast64_t seq_nbr; /*! The globally unique monotic sequence
                                       *  number. */
        ck_stack_t stack; /*! The frame descriptor stack. */
        frame_t *frames; /*! The frame descriptor pool. */
} frames_t;

/**
 * Initialize frames_t.
 */
#define FRAMES_INITIALIZER = {0, 0, 0, CK_STACK_INITIALIZER, NULL};

/**
 * The global frame pool.
 *
 * All frame descriptors come from this pool, but they're small, and so we
 * don't need to carry around multiple blocks of frame descriptors.
 *
 * running: If set, we're running actively, and frames can be pushed and
 * popped. If cleared, we're shutting down, and only pushes are allowed until
 * we've pushed all frames.
 */
typedef struct {
        running_t running; /*! Manage the running state. */
        frames_t frames; /*! The frames stack. */
} frame_pool_t;

/**
 * Initialize the global frame pool with the requested number of frames.
 */
frame_pool_t *frame_pool_init(uint32_t);

/**
 * Return true if we're running, false if not.
 */
inline bool frame_pool_running(const frame_pool_t const *p) {
        return atomic_load(&(p->running.running));
}

/**
 * Set that we're shutting down, then wait for the pool to be finished.
 *
 * Returns true if we could shut the pool down, false if not.
 */
bool frame_pool_shutdown(frame_pool_t *p);

/**
 * True if all frames are in the pool.
 */
inline bool frame_pool_full(const frame_pool_t *p) {
        return (atomic_load(&p->frames.n_frames_avail) == p->frames.n_frames);
}

/**
 * Push a frame back into the pool.
 *
 * This reaps the frame body, adds it back to the pool, and updates
 * n_frames_avail.
 *
 * If we're not running and all the frames have been returned, this
 * also will signal all the waiters on shutdown_cnd.
 */
inline void frame_pool_push(frame_pool_t *p, frame_t *f) {
        reap_frame(f);

        ck_stack_push_mpmc(&(p->frames.stack), &(f->f_entry));
        atomic_fetch_add(&(p->frames.n_frames_avail), 1);

        if ((!frame_pool_running(p)) && frame_pool_full(p)) {
                cnd_broadcast(&(p->running.shutdown_cnd));
        }
};

/**
 * Pop the next frame from the frame stack.
 *
 * Increments the sequence number and sets it on the frame, then updates
 * n_frames_avail.
 *
 * If we're stopping, or out of frames, returns NULL.
 */
inline frame_t *frame_pool_pop(frame_pool_t *p) {
        if (!frame_pool_running(p))
                return NULL;

        frame_t *f = (frame_t *)ck_stack_pop_mpmc(&(p->frames.stack));

        if (!f) return NULL;

        f->f_seq = atomic_fetch_add(&(p->frames.seq_nbr), 1);
        atomic_fetch_add(&(p->frames.n_frames_avail), -1);

        return f;
};

#endif
