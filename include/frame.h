/**
 * Frame structures.
 */

#ifndef _frame_h_
#define _frame_h_

#include <libaudit.h>
#include <ck_stack.h>
#include <ck_queue.h>
#include <jansson.h>
#include "fmalloc.h"

/**
 * The payload type of this frame
 */
typedef enum {
        F_EMPTY = 0, /*! An empty frame. */
        F_AUDIT = 1, /*! An audit message frame */
        F_JSON = 2, /*! A JSON frame. */
        F_JSON_S = 3 /*! A serialized JSON frame. */
} frame_type_t;


/**
 * An audit message.
 */
typedef struct {
        struct audit_dispatcher_header hdr; /*! The audit frame header. */
        uint8_t data[MAX_AUDIT_MESSAGE_LENGTH]; /*! The audit message. */
} f_audit_t;

/**
 * The body of the frame.
 *
 * All dynamically allocated data is allocated from a thread-managed pool,
 * usually automatically by mallocx.
 */
typedef union {
        f_audit_t *f_audit; /*! A frame from the audit subsystem. */
        json_t *f_json; /*! A frame containing the JSON representation of
                             *  an audit_f. */
        char *f_json_s; /*! A frame containing the JSON string to be sent over HTTP/2. */
} frame_body_t;

/**
 * A frame for any of the frame stacks.
 *
 * A frame is a request for work by one of the worker threads. While
 * in progress, it has a unique sequence number that's monotonically
 * increasing, and a payload determined by the f_type field.
 */
typedef struct {
        ck_stack_entry_t f_entry; /*! The stack entry for this
                                   *  frame. Also at the head so that
                                   *  the base address is the same as
                                   *  the frame_t. */

        uint64_t f_seq; /*! The globally unique frame sequence number. */
        frame_type_t f_type; /*! The contents of the body. */
        frame_body_t f_body; /*! The body itself. */
} frame_t;

/**
 * Initialize a frame of the requested type.
 *
 * If the frame has a source frame, it should be in s. Currently, only
 * F_JSON_S frames have one, and it must be a F_JSON frame.
 *
 * Returns NULL on failure, f on success.
 */
inline frame_t *init_frame(frame_t *f, const frame_t * const s) {
        switch (f->f_type) {
        case F_AUDIT:
                if ((f->f_body.f_audit = gmalloc(1, sizeof(f_audit_t), 0)) == NULL)
                        return NULL;
                break;
        case F_JSON:
                f->f_body.f_json = NULL;
                break;
        case F_JSON_S:
                if (s->f_type != F_JSON) return NULL;
                if ((f->f_body.f_json_s = json_dumps(s->f_body.f_json, JSON_INDENT(2))) == NULL)
                        return NULL;
                break;
        }

        return f;
}
/**
 * Reap a frame given its type.
 */
inline frame_t *reap_frame(frame_t *f) {
        switch (f->f_type) {
        case F_AUDIT:
                free(f->f_body.f_audit);
                break;
        case F_JSON:
                json_decref(f->f_body.f_json);
                break;
        case F_JSON_S:
                free(f->f_body.f_json_s);
                break;
        };

        memset(f, 0, sizeof(frame_t));
        return f;
};

#endif
