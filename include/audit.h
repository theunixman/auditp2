/** \file audit.h
 *
 * The audit thread control structures.
 */

#include "threads.h"
#include "frame_pool.h"
#include "fmalloc.h"

/**
 * Returns the information on the running audit thread.
 *
 * The frame_pool can be used to notify it to shut down, and the
 * audit_thr can then be waited on to check the shutdown status.
 */
typedef struct {
        thrd_t audit_thr;
        frame_pool_t frame_pool;
} audit_t;

/**
 * Run the audit thread, and return the audit_t control structure.
 */
audit_t *audit_start();

/**
 * Wait for the audit thread to terminate and report the status.
 *
 * Returns -1 on join failure, otherwise the return value from the
 * thread.
 */
inline int audit_wait(audit_t *a) {
        int r;

        if (thrd_join(a->audit_thr, &r) != thrd_success) {
                log_error(__func__, "thrd_join audit_thr", errno);
                return -1;
        }

        return r;
};

/**
 * Exit conditions for the audit thread.
 */
typedef enum {
        AUDIT_THR_OK = 0,
        AUDIT_THR_FATAL = 1,
        AUDIT_THR_RETRY = 2
} audit_thr_exit_t;
