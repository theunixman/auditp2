/** \file worker.h
 *
 *
 * \brief This defines the interface for sending frames to workers for
 * processing.
 */

#include <threads.h>
#include <stdint.h>

#include "frame_pool.h"

typedef workers_t;

/**
 * A worker control block.
 *
 * This handles the communication between the worker, the global
 * frame_stack, and the scheduler.
 */
typedef struct {
        ck_stack_entry_t w_entry; /*! The stack entry for this worker. */
        const frame_pool_t *w_pool; /*! The global frame pool. */
        frame_stack_t *w_stack; /*! The worker thread stack. */
        cnd_t wt_started; /*! The condition variable notifying the thread is ready or failed. */
        thrd_t wt_thrd; /*! The worker thread handle. */
} worker_t;

/**
 * The worker_t structures are kept in a ck_fifo_t.
 *
 * The scheduler works as follows:
 *
 * 1. It takes the time of the last job submission and windows it by 0.1s intervals.
 * 2. It then finds the shortest queue in the most recent window.
 * 3. If any queue is longer than 5 entries, it moves to the next window.
 * 4. This adding 5 continues until a worker is found.
 */
typedef struct {
        ck_fifo_t workers;
} workers_t;

/**
 * Start worker pool, wait for it to begin, check its status, and return it.
 */
workers_t *start_workers(const frame_pool_t *p, uint32_t n);
